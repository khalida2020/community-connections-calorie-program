/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calories;

/**
 *
 * @author Khalid
 */
public class Calculator {
    public static double calculateM (double weight, double height, double age, double active){
            double caloriesM = (66.5 + (13.8*weight) + (5*height) - (6.8*age))*active;
            return caloriesM;                   
    }
    public static double calculateF (double weight, double height, double age, double active){
            double caloriesF = (655.1 + (9.6*weight) + (1.9*height) - (4.7*age))*active;
            return caloriesF; 

    }
}