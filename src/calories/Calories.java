/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calories;

import javax.swing.JOptionPane;

/**
 *
 * @author Khalid
 */
public class Calories {

    /**
     * @param args the command line arguments
     */
    
      public static void main(String[] args) {
        Calculator method = new Calculator();
        JOptionPane.showMessageDialog(null, "Welcome to the Daily Calorie Recommendation Calculator", 
                "Calorie Requirement Calculator", 
                JOptionPane.INFORMATION_MESSAGE);
        String [] inputs = {"Male", 
            "Female"
            };
        
        
        String [] activityLevels = {"Sedentary", 
            "Moderatly Active",
            "Active"
            };
        
        double activityValue = 0;   
        
        String loop = "Yes";
      
        while (loop.equals("Yes")) {
        
        String chosenGender = (String) JOptionPane.showInputDialog(null,
                "Choose your Gender from the Drop Down Menu",
                "Calorie Requirement Calculator",
                JOptionPane.INFORMATION_MESSAGE, null, inputs, inputs[1]);
        
        if (chosenGender.equals("Male")){
            double weight = Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Weight (Kg)", 
                                                                            "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            double height= Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Height (cm)", 
                                                                            "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            double age = Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Age", 
                                                                            "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            
            String activityLevel = (String) JOptionPane.showInputDialog(null,
                "Choose the Activity Level that Best Applies to you",
                "Calorie Requirement Calculator",
                JOptionPane.INFORMATION_MESSAGE, null, activityLevels, activityLevels[1]);
            
            double active = ValueAssigner.assign (activityLevel,activityValue);
            double caloriesM = Math.round(Calculator.calculateM (weight,height,age, active));
              
            JOptionPane.showMessageDialog(null, ("Your Daily Required Calories is: " + (int) caloriesM + " Calories"), 
                                              "Calorie Requirement Calculator", JOptionPane.INFORMATION_MESSAGE);
        }
        if (chosenGender.equals("Female")){
            double weight = Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Weight (Kg)",
                                                                            "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            double height= Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Height (cm)", 
                                                                            "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            double age = Double.parseDouble(JOptionPane.showInputDialog(null, "Please Enter Your Age", 
                                                                           "Calorie Requirement Calculator",JOptionPane.PLAIN_MESSAGE));
            
            String activityLevel = (String) JOptionPane.showInputDialog(null,
                "Choose the Activity Level that Best Applies to you",
                "Calorie Requirement Calculator",
                JOptionPane.INFORMATION_MESSAGE, null, activityLevels, activityLevels[1]);
            
            double active = ValueAssigner.assign (activityLevel,activityValue); 
            double caloriesF = Math.round(Calculator.calculateF (weight,height,age, active)) ;
            
            JOptionPane.showMessageDialog(null, ("Your Daily Required Calories is: " + (int) caloriesF + " Calories"), 
                                             "Calorie Requirement Calculator", JOptionPane.INFORMATION_MESSAGE);
        }
        String [] options = {"Yes", "No, Close the App",};
          loop = (String) JOptionPane.showInputDialog(null,
                "Do you want to restart for another Calculation?",
                "Calorie Requirement Calculator",
                JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);
        }
      }
}

