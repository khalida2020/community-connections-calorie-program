/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calories;

/**
 *
 * @author Khalid
 */
public class ValueAssigner {
    public static double assign (String activityLevel, double activityValue){
            
        if (activityLevel.equals("Sedentary"))      
            activityValue = 1.2;
      
        if (activityLevel.equals("Moderatly Active"))  
            activityValue = 1.3;
            
        if (activityLevel.equals("Active")) 
            activityValue = 1.4;
        
        return activityValue;
    }   
}
